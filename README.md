# IdanTech Mission

This is home mission for getting to work at Idan Tech Mobile App's Company.

## Requirements

1. The full specification documentation and design screen-shouts are located at` <cwd-this-repositry>/docs`.
2. This app should be implemented using Google's Flutter technology.



## Implementation

This implementation respects the Redux state management architecture - as described at:   
https://pub.dev/packages/redux



### Dependencies Libraries:

#### In runtime

`flutter_redux` - for state management.    
`equatable` - for auto implementation of hash codes to compare between instances.   
`redux_dev_tools` - for debugging with a time-line actions the redux architecture.   
`http` - for making API calls to server.   
`form_field_validator` - to get implemented validator for form fields.   
`grafpix` - for loading bars and icons.   
`redux_persist_flutter` - for making the app state saved on any action and manage login/logout.   
`json_annotation` - to create auto generated methods toJson and fromJson for class.   
`redux_thunk` - to make async actions in redux.   
`flutter_redux_navigation` - to easy rout between screens with respect to redux.   
`share` - to share data with other apps.   

#### In compile time

`json_serializable: ^3.2.5` - used only in compile time to auto generated code for toJson and fromJson for annotated class(should be removed when you running the app(comment it)).   
`build_runner: ^1.0.0` - the generator of the serializable class.   