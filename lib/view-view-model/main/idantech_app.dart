import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/view-view-model/form-business-registration/fetching_login_data_page.dart';
import 'package:idantech_mission/view-view-model/form-business-registration/registration_page.dart';
import 'package:idantech_mission/view-view-model/form-business-registration/submiting_page.dart';
import 'package:idantech_mission/view-view-model/main-page/main_page.dart';
import 'package:redux/redux.dart';

class IdanTechMissionApp extends StatefulWidget {
  final Store<AppState> store;
  const IdanTechMissionApp(this.store);

  @override
  _IdanTechMissionAppState createState() => _IdanTechMissionAppState();
}

class _IdanTechMissionAppState extends State<IdanTechMissionApp> {
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: MaterialApp(
        title: 'RegistrationPage',
        navigatorKey: NavigatorHolder.navigatorKey,
        onGenerateRoute: _getRoute,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: Colors.blue,
        ),
        home: StoreBuilder<AppState>(
            onInit: (store) => {},
            builder: (context, store) {
              return FetchingLoginData();
            }),
      ),
    );
  }


  Route _getRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/RegistrationPage':
        return _buildRoute(settings, new RegistrationPage());
      case '/MainPage':
        return _buildRoute(settings, new MainPage());
      case '/SubmitingLoadingPage':
        return _buildRoute(settings, new SubmitingLoadPage());
      default /*'/FetchingLoginData'*/:
        return _buildRoute(settings, FetchingLoginData());
    }
  }


  MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
    return new MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => builder,
    );
  }
}
