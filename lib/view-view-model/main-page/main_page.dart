//"${viewModel.submitState}"
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/model/client-api-models/business_details_model.dart';
import 'package:idantech_mission/model/client-api-models/server_add_business_respons.dart';
import 'package:redux/redux.dart';
import 'package:share/share.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MainModel>(
        converter: (store) => MainModel.build(store),
        builder: (context, viewModel) {
          return Scaffold(
            appBar: AppBar(title: Text("Business Deshboard")),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Share.share('Its IdanTech Business!\n Business name: ${viewModel.businessName} \n Business ID: ${viewModel.businessId}');
              },
              child: Icon(Icons.share),
              splashColor: Colors.amberAccent,
              backgroundColor: Colors.amber,
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Bussiness Logged In With ID:"),
                  Text("${viewModel.businessId}"),
                ],
              ),
            ),
          );
        });
  }
}

class MainModel {
  //state:
  final int businessId;
  final String businessName;

  MainModel({this.businessId, this.businessName});

  static MainModel build(Store<AppState> store) {
    return MainModel(
        businessId: store.state.serverBusinessDetails.businessId,
        businessName: store.state.bussinessDetails.bussinessName);
  }
}
