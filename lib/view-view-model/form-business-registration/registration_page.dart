import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:idantech_mission/configurations/config.dart';
import 'package:idantech_mission/custum_widgets/titled_border.dart';
import 'package:idantech_mission/custum_widgets/validators/custum_validators.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/model/client-api-models/business_details_model.dart';
import 'package:idantech_mission/redux/actions.dart';
import 'package:idantech_mission/view-view-model/form-business-registration/submiting_page.dart';
import 'package:redux/redux.dart';
import 'package:idantech_mission/view-view-model/main-page/main_page.dart';


class RegistrationPage extends StatefulWidget {
  RegistrationPage({Key key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _onInfo() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
        content:
            Text("please add business details and click save to register")));
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SubmitRegistrationModel>(
      converter: (store) => SubmitRegistrationModel.build(store),
      builder: (context, viewModel) {
        return Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text('Business Details'),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.info),
                  onPressed: () {
                    viewModel.onInfo(_onInfo);
                  },
                ),
              ],
            ),
            body: new ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  child: new SubmitForm(),
                ),
              ],
            ));
      },
    );
  }
}


class SubmitForm extends StatefulWidget {
  SubmitForm({Key key}) : super(key: key);

  @override
  _SubmitFormState createState() => _SubmitFormState();
}

class _SubmitFormState extends State<SubmitForm> {
  @override
  Widget build(BuildContext context) {
    final bussinessNameController = TextEditingController();
    final phoneNumberController = TextEditingController();
    final emailController = TextEditingController();
    final countryController = TextEditingController();
    final cityController = TextEditingController();
    final addressController = TextEditingController();

    final _formKey = GlobalKey<FormState>();
    return StoreConnector<AppState, SubmitRegistrationModel>(
      converter: (store) => SubmitRegistrationModel.build(store),
      builder: (context, viewModel) {

        return new Form(
          key: _formKey,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                SizedBox(height: 10),
                TitledBoarderFormField(
                  controller: bussinessNameController,
                  icon: Icons.account_circle,
                  title: "Business Name",
                  validator: MinLengthValidator(3,
                      errorText: 'business name must be at least 3 chars long'),
                ),
                SizedBox(height: 10),
                TitledBoarderFormField(
                  icon: Icons.phone,
                  controller: phoneNumberController,
                  title: "Phone Number",
                  keyboardType: TextInputType.phone,
                  validator: LYDPhoneValidator(errorText: "Phone not valid"),
                ),
                SizedBox(height: 10),
                TitledBoarderFormField(
                  icon: Icons.email,
                  controller: emailController,
                  title: "Email",
                  keyboardType: TextInputType.emailAddress,
                ),
                SizedBox(height: 10),
                TitledBoarderFormField(
                  icon: Icons.flag,
                  controller: countryController,
                  title: "Country",
                  keyboardType: TextInputType.text,
                  validator: MinLengthValidator(3,
                      errorText: 'country name must be at least 3 chars long'),
                ),
                SizedBox(height: 10),
                TitledBoarderFormField(
                  icon: Icons.location_city,
                  controller: cityController,
                  title: "City",
                  validator: MinLengthValidator(3,
                      errorText: 'city name must be at least 3 chars long'),
                ),
                SizedBox(height: 10),
                TitledBoarderFormField(
                  icon: Icons.home,
                  controller: addressController,
                  title: "Address",
                  validator: MinLengthValidator(5,
                      errorText: 'Address name must be at least 5 chars long'),
                ),
                SizedBox(height: 10),
                Container(
                  height: 50,
                  width: 300,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    color: Config.FORGROUND_REGISTRATION_COLOR,
                    child: Text("Save",
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 255, 1), fontSize: 20)),
                    onPressed: () {
                     if (_formKey.currentState.validate()) {
                        viewModel.onSubmit(
                          BussinessDetails(
                            bussinessName: bussinessNameController.text,
                            phoneNumber: phoneNumberController.text,
                            email: emailController.text,
                            country: countryController.text,
                            city: cityController.text,
                            address: addressController.text,
                          ),
                          () {
                            /*go to main page*/
                            Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MainPage()),
                              (Route<dynamic> route) => false,
                            );
                          },
                          () {
                            /*pop dialog*/
                            Navigator.pop(context);
                          },
                        );
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SubmitingLoadPage()),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

typedef void VoidCallback();
typedef OnSubmit = Function(BussinessDetails bussinessDetails,
    VoidCallback onSuccsess, VoidCallback onFaild);
typedef OnIcon = Function(VoidCallback);

class SubmitRegistrationModel {
  //state:
  final BussinessDetails bussinessDetails;
  final bool isLoggedIn;
  //callbacks
  final OnSubmit onSubmit;
  final OnIcon onInfo;

  SubmitRegistrationModel({
    this.bussinessDetails,
    this.isLoggedIn,
    this.onSubmit,
    this.onInfo,
  });

  static SubmitRegistrationModel build(Store<AppState> store) {
    return SubmitRegistrationModel(
      bussinessDetails: store.state.bussinessDetails,
      isLoggedIn : store.state.isLoggedIn,
      onSubmit: (BussinessDetails item, VoidCallback onSuccsess,
          VoidCallback onFaild) {
        store.dispatch(SubmitBussinessDetails(item, onSuccsess, onFaild));
      },
      onInfo: (VoidCallback showSnackBar) {
        store.dispatch(ShowSnackBarAction(showSnackBar));
      },
    );
  }
}
