//"${viewModel.submitState}"
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:grafpix/pixloaders/pix_loader.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/redux/actions.dart';
import 'package:idantech_mission/view-view-model/form-business-registration/registration_page.dart';
import 'package:idantech_mission/view-view-model/main-page/main_page.dart';
import 'package:redux/redux.dart';

class FetchingLoginData extends StatefulWidget {
  FetchingLoginData({Key key}) : super(key: key);

  @override
  _FetchingLoginState createState() => _FetchingLoginState();
}

class _FetchingLoginState extends State<FetchingLoginData> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, FetchingLoginDataModel>(
        converter: (store) => FetchingLoginDataModel.build(store),
        builder: (context, viewModel) {
          viewModel.fetchBusiness(
              /*viewModel.nevigateToMainPage*/
              () {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => MainPage()),
              (Route<dynamic> route) => false,
            );
          },
              /*  viewModel.nevigateToRegistrationPage); */
              () {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => RegistrationPage()),
              (Route<dynamic> route) => false,
            );
          });

          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  PixLoader(
                      loaderType: LoaderType.Spinner, faceColor: Colors.white),
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    "Idan Tech!",
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
            ),
          );
        });
  }
}

typedef OnFetch = Function(VoidCallback onSuccsess, VoidCallback onFaild);
typedef OnNevigation = Function();

class FetchingLoginDataModel {
  //state:
  final bool isLoggedIn;
  final OnFetch fetchBusiness;
  final OnNevigation nevigateToMainPage;
  final OnNevigation nevigateToRegistrationPage;

  FetchingLoginDataModel(
      {this.isLoggedIn,
      this.fetchBusiness,
      this.nevigateToMainPage,
      this.nevigateToRegistrationPage});

  static FetchingLoginDataModel build(Store<AppState> store) {
    return FetchingLoginDataModel(
      isLoggedIn: store.state.isLoggedIn,
      fetchBusiness: (VoidCallback onSuccess, VoidCallback onFail) {
        store.dispatch(businessFetchedAction(onSuccess, onFail));
      },
      nevigateToMainPage: () {
        //Not In Use! - this because it create navigation twice
        //check out the issue on git about it: https://github.com/flutterings/flutter_redux_navigation/issues/16
        store.dispatch(NavigateToAction.replace("/MainPage"));
      },
      nevigateToRegistrationPage: () {
        //Not In Use! - this because it create navigation twice
        //check out the issue on git about it: https://github.com/flutterings/flutter_redux_navigation/issues/16
        store.dispatch(NavigateToAction.replace("/RegistrationPage"));
      },
    );
  }
}
