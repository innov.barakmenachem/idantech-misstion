//"${viewModel.submitState}"
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:grafpix/pixloaders/pix_loader.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/model/submit_state.dart';
import 'package:redux/redux.dart';

class SubmitingLoadPage extends StatefulWidget {
  SubmitingLoadPage({Key key}) : super(key: key);

  @override
  _SubmitFormState createState() => _SubmitFormState();
}

class _SubmitFormState extends State<SubmitingLoadPage> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, SubmitLoadModel>(
        converter: (store) => SubmitLoadModel.build(store),
        builder: (context, viewModel) {
          return Scaffold(
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  PixLoader(
                      loaderType: LoaderType.Rocks, faceColor: Colors.white),
                  SizedBox(
                    height: 40,
                  ),
                  Text("${viewModel.submitState.toString()}")
                ],
              ),
            ),
          );
        });
  }
}

class SubmitLoadModel {
  //state:
  final SubmitState submitState;

  SubmitLoadModel({
    this.submitState,
  });

  static SubmitLoadModel build(Store<AppState> store) {
    return SubmitLoadModel(
      submitState: store.state.submitState,
    );
  }
}
