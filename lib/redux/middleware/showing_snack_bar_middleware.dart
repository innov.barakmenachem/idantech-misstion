import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/redux/actions.dart';
import 'package:redux/redux.dart';

class ShowingSnackBarMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    next(action);

    if (action is ShowSnackBarAction) {
      _onSnackBar(store, action);
    }
  }

  Future _onSnackBar(Store<AppState> store, ShowSnackBarAction action) async {
    //async operation
    action.showInfo();
  }
}
