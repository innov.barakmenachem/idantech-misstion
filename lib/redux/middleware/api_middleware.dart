import 'package:idantech_mission/API/api_client.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/model/client-api-models/server_add_business_respons.dart';
import 'package:idantech_mission/model/submit_state.dart';
import 'package:idantech_mission/redux/actions.dart';
import 'package:redux/redux.dart';

class ApiMiddleware extends MiddlewareClass<AppState> {
  final ApiClient apiClient;

  ApiMiddleware(this.apiClient);

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    if (action is SubmitBussinessDetails) {
      _onSubmitBussinessDetails(store, action);
    }

    next(action);
  }

  Future _onSubmitBussinessDetails(
      Store<AppState> store, SubmitBussinessDetails action) async {
    //async operation
    store.dispatch(UpdateSubmitStateAction(SubmitState(SubmitType.SAVING)));
    await new Future.delayed(new Duration(seconds: 2), () async {
      AddBusinessResponse addBusinessResponseContent =
          await apiClient.submitBussinessDetails(action.bussinessDetails);

      store.dispatch(
          UpdateBusinessResponseDataAction(addBusinessResponseContent));

      SubmitState newState =
          SubmitState.fromAddBusinessResponse(addBusinessResponseContent);

      if (newState.currentSubmitState == SubmitType.FAILD_TO_SAVE) {
        await new Future.delayed(
          new Duration(seconds: 2),
          () async {
            action.onFail();
          },
        );
      } else if (newState.currentSubmitState == SubmitType.SAVED) {
        await new Future.delayed(new Duration(seconds: 2), () async {
          action.onSuccess();
        });
      }
    }); /* delay for user ux */
  }
}
