import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';

/* get state and reduce the action changes on it.
 * 
 * @param   state       current state.
 * @param   action      an 'ReducerAction', the action we want to use.
 */
AppState appStateReducers(AppState state, dynamic action) {
  if (action is! DevToolsAction && action is! NavigateToAction) {
    return action.reduce(state);
  } else {
    return state;
  }
}
