import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/model/client-api-models/business_details_model.dart';
import 'package:idantech_mission/model/client-api-models/server_add_business_respons.dart';
import 'package:idantech_mission/model/submit_state.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:redux/redux.dart';

abstract class ReducerAction extends Equatable {
  // This class is intended to be used as a mixin, and should not be
  // extended directly.

  AppState reduce(AppState state);
}

/* Setting the state of the gate(close -> openinig | opening -> open | etc.)
 * 
 */
class SubmitBussinessDetails extends ReducerAction {
  final BussinessDetails bussinessDetails;
  final VoidCallback onSuccess;
  final VoidCallback onFail;

  SubmitBussinessDetails(this.bussinessDetails, this.onSuccess, this.onFail);

  @override
  List<Object> get props => [bussinessDetails, onSuccess, onFail];

  @override
  AppState reduce(AppState state) {
    return AppState.fromBussinessDetails(state, bussinessDetails);
  }
}

/* Setting the state of the gate(close -> openinig | opening -> open | etc.)
 * 
 */
class UpdateSubmitStateAction extends ReducerAction {
  final SubmitState submitState;

  UpdateSubmitStateAction(this.submitState);

  @override
  List<Object> get props => [submitState];

  @override
  AppState reduce(AppState state) {
    return AppState.fromSubmitState(state, submitState);
  }
}

class UpdateBusinessResponseDataAction extends ReducerAction {
  final AddBusinessResponse addBusinessResponse;

  UpdateBusinessResponseDataAction(this.addBusinessResponse);

  @override
  List<Object> get props => [addBusinessResponse];

  @override
  AppState reduce(AppState state) {
    return AppState.fromAddBusinessResponse(state, addBusinessResponse);
  }
}

/* Action for showing snack bar
 * 
 */
class ShowSnackBarAction extends ReducerAction {
  final VoidCallback showInfo;

  ShowSnackBarAction(this.showInfo);

  @override
  List<Object> get props => [showInfo];

  @override
  AppState reduce(AppState state) {
    return state;
  }

  void showSnackBar() {
    showInfo();
  }
}

/* Action that check if bussiness is logged in or not
 *
 */
ThunkAction<AppState> businessFetchedAction(
    VoidCallback onSuccess, VoidCallback onFail) {
  return (Store<AppState> store) async {
    await new Future.delayed(new Duration(milliseconds: 1000));
    if (store.state.isLoggedIn) {
      onSuccess();
    } else {
      onFail();
    }
  };
}
