import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:idantech_mission/API/api_client.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/redux/middleware/api_middleware.dart';
import 'package:idantech_mission/redux/middleware/logger_middleware.dart';
import 'package:idantech_mission/redux/middleware/showing_snack_bar_middleware.dart';
import 'package:idantech_mission/redux/reducers.dart';
import 'package:redux/redux.dart';
import 'package:redux_dev_tools/redux_dev_tools.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

Future<Store<AppState>> createReduxStore() async {
   // Create Persistor
  final persistor = Persistor<AppState>(
    storage: FlutterStorage(),
    serializer: JsonSerializer<AppState>(AppState.fromJson),
  );
  
  // Load initial state
  final initialState = await persistor.load();

  final apiClient = ApiClient();//ApiClientMock()
  

  return DevToolsStore<AppState>(
    combineReducers<AppState>([appStateReducers]),
    initialState: initialState ?? AppState.empty(),
    middleware: [
      persistor.createMiddleware(),
      NavigationMiddleware(),
      thunkMiddleware,
      ShowingSnackBarMiddleware(),
      ApiMiddleware(apiClient),
      LoggerMiddleware()
    ],
  );
}
