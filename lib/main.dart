import 'package:flutter/material.dart';
import 'package:idantech_mission/model/app_state.dart';
import 'package:idantech_mission/redux/store.dart';
import 'package:idantech_mission/view-view-model/main/idantech_app.dart';

/* 
 * State managment is architecured with :
 * [flutter_redux](https://pub.dev/packages/flutter_redux) 
 * 
 * and tested with :
 * (flutter_redux_dev_tools)[https://pub.dev/packages/flutter_redux_dev_tools].
 * 
 * Thanks for:
 * [@pszklarska](https://github.com/pszklarska) 
 * 
 * for the great redux example:
 * [shopping_cart](https://github.com/pszklarska/flutter_shopping_cart).
 * 
 * 
 * created by Barak Menachem
*/
void main() async {
  //must ensure initialiation before reading from shared_preferencies when creating the store state
  WidgetsFlutterBinding.ensureInitialized();
  final store = await createReduxStore();
  runApp(IdanTechMissionApp(store));
}
