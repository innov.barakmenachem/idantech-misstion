/*
 * Static class that contains all of the constants such as server url
 */
import 'package:flutter/material.dart';

class Config {
  /* Client API */

  //http standarts
  static const int HTTP_OK_STATUS = 200;
  //server add business url
  static const String ADD_BUSSINESS__SERVER_URL =
      'https://mybusiness.idantech.co.il/app/business/add-business.php';
  //name of the retured array after add business
  static const String ADD_BUSINESS__SERVER_RESPONSE = "server_response";

  static const String BUSINESS_ADD_SUCCSESS_FLAG = "true";

  /* Colors */
  //forground registration color
  static const Color FORGROUND_REGISTRATION_COLOR = Colors.white;
}
