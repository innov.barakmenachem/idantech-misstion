import 'package:form_field_validator/form_field_validator.dart';

class LYDPhoneValidator extends TextFieldValidator {  
  // pass the error text to the super constructor  
  LYDPhoneValidator({String errorText = 'enter a valid LYD phone number'}) : super(errorText);  

  @override  
   bool get ignoreEmptyValues => false;  
    
	  @override  
	  bool isValid(String value) {  
	    // return true if the value is valid according the your condition  
	    return hasMatch(r'^(?:[+0]9)?[0-9]{10}$', value);  
	  }  
}    