// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppState _$AppStateFromJson(Map<String, dynamic> json) {
  return AppState(
    json['submitState'] == null
        ? null
        : SubmitState.fromJson(json['submitState'] as Map<String, dynamic>),
    json['bussinessDetails'] == null
        ? null
        : BussinessDetails.fromJson(
            json['bussinessDetails'] as Map<String, dynamic>),
    json['isLoggedIn'] as bool,
    json['serverBusinessDetails'] == null
        ? null
        : AddBusinessResponse.fromJson(
            json['serverBusinessDetails'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AppStateToJson(AppState instance) => <String, dynamic>{
      'submitState': instance.submitState,
      'bussinessDetails': instance.bussinessDetails,
      'isLoggedIn': instance.isLoggedIn,
      'serverBusinessDetails': instance.serverBusinessDetails,
    };
