import 'package:equatable/equatable.dart';
import 'package:idantech_mission/model/client-api-models/business_details_model.dart';
import 'package:idantech_mission/model/client-api-models/server_add_business_respons.dart';
import 'package:idantech_mission/model/submit_state.dart';
import 'package:json_annotation/json_annotation.dart';
part 'app_state.g.dart';

/* Json serializable working flow explained here:
 * https://medium.com/flutter-community/generate-the-code-to-parse-your-json-in-flutter-c68aa89a81d9
 * 
 */
@JsonSerializable(nullable: true)
class AppState extends Equatable {
  final SubmitState submitState;
  final BussinessDetails bussinessDetails;
  final bool isLoggedIn;
  final AddBusinessResponse serverBusinessDetails;

  AppState(this.submitState, this.bussinessDetails, this.isLoggedIn,
      this.serverBusinessDetails);

  AppState.fromBussinessDetails(
      AppState state, BussinessDetails submitedBussinessDatails)
      : submitState = state.submitState,
        bussinessDetails = submitedBussinessDatails,
        isLoggedIn = state.isLoggedIn,
        serverBusinessDetails = state.serverBusinessDetails;

  AppState.fromSubmitState(AppState state, SubmitState newSubmitStateState)
      : submitState = newSubmitStateState,
        bussinessDetails = state.bussinessDetails,
        isLoggedIn = state.isLoggedIn,
        serverBusinessDetails = state.serverBusinessDetails;

  AppState.fromAddBusinessResponse(
      AppState state, AddBusinessResponse serverBusinessDetails)
      : submitState =
            SubmitState.fromAddBusinessResponse(serverBusinessDetails),
        bussinessDetails = state.bussinessDetails,
        isLoggedIn = serverBusinessDetails.result,
        serverBusinessDetails = serverBusinessDetails;

  static AppState fromJson(dynamic json) {
    if (json == null) {
      return AppState.empty();
    } else {
      return _$AppStateFromJson(json);
    }
  }

  factory AppState.empty() => AppState(
        new SubmitState(SubmitType.IDLE),
        new BussinessDetails(),
        false,
        AddBusinessResponse(),
      );

  Map<String, dynamic> toJson() => _$AppStateToJson(this);

  @override
  String toString() =>
      "$submitState |||| $bussinessDetails |||| $isLoggedIn |||| $serverBusinessDetails";

  @override
  List<Object> get props =>
      [submitState, bussinessDetails, isLoggedIn, serverBusinessDetails];
}
