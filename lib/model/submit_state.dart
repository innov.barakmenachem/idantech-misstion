import 'package:equatable/equatable.dart';
import 'package:idantech_mission/model/client-api-models/server_add_business_respons.dart';

enum SubmitType {
  IDLE,
  SAVING,
  SAVED,
  FAILD_TO_SAVE,
}

class SubmitState extends Equatable {
  final SubmitType currentSubmitState;

  SubmitState(this.currentSubmitState);

  SubmitState.fromJson(Map<String, dynamic> json)
      : currentSubmitState = SubmitType.values[json['currentSubmitState']];

  factory SubmitState.fromAddBusinessResponse(AddBusinessResponse response) {
    return SubmitState(
        response.result ? SubmitType.SAVED : SubmitType.FAILD_TO_SAVE);
  }

  Map<String, dynamic> toJson() => {'currentSubmitState': currentSubmitState.index};

  @override
  String toString() {
    if (currentSubmitState == SubmitType.FAILD_TO_SAVE) {
      return "Failed to Save";
    }
    if (currentSubmitState == SubmitType.SAVED) {
      return "Saved";
    }
    if (currentSubmitState == SubmitType.SAVING) {
      return "Saving";
    }
    return "idle";
  }

  @override
  List<Object> get props => [currentSubmitState];
}
