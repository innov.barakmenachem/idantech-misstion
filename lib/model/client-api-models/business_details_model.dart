import 'package:equatable/equatable.dart';

class BussinessDetails extends Equatable {
  final String bussinessName;
  final String phoneNumber;
  final String email;
  final String country;
  final String city;
  final String address;

  BussinessDetails({
    this.bussinessName,
    this.phoneNumber,
    this.email,
    this.country,
    this.city,
    this.address,
  });

  BussinessDetails.fromJson(Map<String, dynamic> json)
      : bussinessName = json['bussinessName'],
        phoneNumber = json['phoneNumber'],
        email = json['email'],
        country = json['country'],
        city = json['city'],
        address = json['address'];

  Map<String, dynamic> toJson() => {
        'bussinessName': bussinessName,
        'phoneNumber': phoneNumber,
        'email': email,
        'country': country,
        'city': city,
        'address': address,
      };

  @override
  String toString() {
    return "\{ " +
        "$bussinessName ," +
        "$phoneNumber ," +
        "$email ," +
        "$country ," +
        "$city ," +
        "$address" +
        "\}";
  }

  @override
  List<Object> get props => [
        bussinessName,
        phoneNumber,
        email,
        country,
        city,
        address,
      ];
}
