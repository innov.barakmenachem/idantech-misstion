import 'package:equatable/equatable.dart';
import 'package:idantech_mission/configurations/config.dart';


class AddBusinessResponse extends Equatable {
  final bool result;
  final int businessId;

  AddBusinessResponse({
    this.result,
    this.businessId,
  });

  AddBusinessResponse.fromJson(Map<String, dynamic> json)
      : result = json['result'] == Config.BUSINESS_ADD_SUCCSESS_FLAG,
        businessId = json['businessId'];

  Map<String, dynamic> toJson() => {
        'result': result,
        'businessId': businessId
      };

  @override
  String toString() {
    return "\{ " +
        "$result ," +
        "$businessId "
        "\}";
  }

  @override
  List<Object> get props => [
        result,
        businessId,
      ];
}
