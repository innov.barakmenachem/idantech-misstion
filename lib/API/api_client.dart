import 'dart:convert';

import 'package:idantech_mission/configurations/config.dart';
import 'package:idantech_mission/model/client-api-models/business_details_model.dart';
import 'package:idantech_mission/model/client-api-models/server_add_business_respons.dart';
import 'package:idantech_mission/model/submit_state.dart';
import 'package:http/http.dart';

class ApiClientMock {
  ApiClientMock();

  Future<AddBusinessResponse> submitBussinessDetails(
      BussinessDetails bussinessDetails) async {
    AddBusinessResponse addBusinessResponseContent =
        new AddBusinessResponse(result: true, businessId: 5555);
          await Future.delayed(new Duration(seconds: 2), () async {});
    return addBusinessResponseContent;
  }
}

class ApiClient {
  ApiClient();

  Future<AddBusinessResponse> submitBussinessDetails(
      BussinessDetails bussinessDetails) async {
    // set up POST request arguments
    String url = Config.ADD_BUSSINESS__SERVER_URL;
    AddBusinessResponse addBusinessResponseContent =
        new AddBusinessResponse(result: false, businessId: -1);

    Map<String, String> headers = {"Content-type": "application/json"};

    //encode the object to json
    String json = JsonEncoder.withIndent("").convert(bussinessDetails);

    // make POST request
    Response response = await post(url, headers: headers, body: json);

    // check the status code for the result
    int statusCode = response.statusCode;
    if (statusCode != Config.HTTP_OK_STATUS) {
      return addBusinessResponseContent;
    }

    // parsing the response
    String body = response.body;
    var serverResponsArrayJson =
        jsonDecode(body)[Config.ADD_BUSINESS__SERVER_RESPONSE];

    addBusinessResponseContent = AddBusinessResponse.fromJson(serverResponsArrayJson[0]);



    await Future.delayed(new Duration(seconds: 2), () async {});
    return addBusinessResponseContent;
  }
}
